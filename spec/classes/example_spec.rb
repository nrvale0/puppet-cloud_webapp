require 'spec_helper'

describe 'cloud_webapp' do
  context 'supported operating systems' do
    ['Debian', 'RedHat'].each do |osfamily|
      describe "cloud_webapp class without any parameters on #{osfamily}" do
        let(:params) {{ }}
        let(:facts) {{
          :osfamily => osfamily,
        }}

        it { should compile.with_all_deps }

        it { should contain_class('cloud_webapp::params') }
        it { should contain_class('cloud_webapp::install').that_comes_before('cloud_webapp::config') }
        it { should contain_class('cloud_webapp::config') }
        it { should contain_class('cloud_webapp::service').that_subscribes_to('cloud_webapp::config') }

        it { should contain_service('cloud_webapp') }
        it { should contain_package('cloud_webapp').with_ensure('present') }
      end
    end
  end

  context 'unsupported operating system' do
    describe 'cloud_webapp class without any parameters on Solaris/Nexenta' do
      let(:facts) {{
        :osfamily        => 'Solaris',
        :operatingsystem => 'Nexenta',
      }}

      it { expect { should contain_package('cloud_webapp') }.to raise_error(Puppet::Error, /Nexenta not supported/) }
    end
  end
end
