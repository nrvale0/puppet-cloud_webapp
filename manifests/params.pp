# == Class cloud_webapp::params
#
# This class is meant to be called from cloud_webapp.
# It sets variables according to platform.
#
class cloud_webapp::params {
  case $::osfamily {
    'redhat': {}
    default: {
      fail("${::operatingsystem} not supported")
    }
  }
}
