class cloud_webapp (
  $ensure = 'running'
) inherits ::cloud_webapp::params {

  validate_re($ensure, '^(present|absent|running|stopped)$')

  case $ensure {

    'running': {
      class { '::cloud_webapp::nodes': ensure => $ensure, } ~>
      class { '::cloud_webapp::app': ensure => $ensure, } ->
      class { '::cloud_webapp::register': ensure => $ensure, } ->
      Class['::cloud_webapp']
    }

    /^(present|absent|stopped)$/: {
      class { '::cloud_webapp::register': ensure => $ensure, } ->
<<<<<<< HEAD
=======
      class { '::cloud_webapp::app': ensure => $ensure, } ->
>>>>>>> safekeeping
      class { '::cloud_webapp::nodes': ensure => $ensure, } ->
      Class['::cloud_webapp']
    }

    default: { fail("Invalid value \'${ensure}\' for variable 'ensure'!") }
  }
}
