class cloud_webapp::nodes(
  $ensure,
) inherits ::cloud_webapp::params {

  validate_re($ensure, '^(running|present|absent|stopped)$')

  vsphere_vm { '/opdx1/vm/eng/nathan@puppetlabs.com/cloud_webapp_lb0':
    ensure => $ensure,
    memory => '1024',
    cpus => '1',
    source => '/opdx1/vm/eng/templates/el7',
    annotation => 'load balancer for cloud_webapp',
  } -> 

  vsphere_vm { '/opdx1/vm/eng/nathan@puppetlabs.com/cloud_webapp_app0':
    ensure => $ensure,
    memory => '1024',
    cpus => '1',
    source => '/opdx1/vm/eng/templates/el7',
    annotation => 'app server for cloud_webapp',
  } -> 

  vsphere_vm { '/opdx1/vm/eng/nathan@puppetlabs.com/cloud_webapp_app1':
    ensure => $ensure,
    memory => '1024',
    cpus => '1',
    source => '/opdx1/vm/eng/templates/el7',
    annotation => 'app server for cloud_webapp',
  } -> 

  vsphere_vm { '/opdx1/vm/eng/nathan@puppetlabs.com/cloud_webapp_app2':
    ensure => $ensure,
    memory => '1024',
    cpus => '1',
    source => '/opdx1/vm/eng/templates/el7',
    annotation => 'app server for cloud_webapp',
  } -> 

  vsphere_vm { '/opdx1/vm/eng/nathan@puppetlabs.com/cloud_webapp_db0':
    ensure => $ensure,
    memory => '1024',
    cpus => '1',
    source => '/opdx1/vm/eng/templates/el7',
    annotation => 'database server for cloud_webapp',
  }
}
